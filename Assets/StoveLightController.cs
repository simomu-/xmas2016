﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class StoveLightController : MonoBehaviour {

    [SerializeField] float minLightIntensity;
    [SerializeField] float maxLightIntensity;

    Light light;
    float currentLightIntensity;

    void Start(){
        light = GetComponent<Light>();
        StartCoroutine(StoveLightEffectCoroutine());
    }

    void Update() {
        light.intensity = Mathf.Lerp(light.intensity, currentLightIntensity, 3.0f * Time.deltaTime);
    }

    IEnumerator StoveLightEffectCoroutine(){

        while (true) {
            currentLightIntensity = Random.Range(minLightIntensity, maxLightIntensity);

            yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
            
        }
        
    }

}
