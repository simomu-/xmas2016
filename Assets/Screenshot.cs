﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screenshot : MonoBehaviour {

    void Update() {
        if(Application.platform != RuntimePlatform.Android && 
           Application.platform != RuntimePlatform.WebGLPlayer) {

            if (Input.GetKeyDown(KeyCode.Return)) {
                TakeScreenshot();
            }

            //if (Input.touchCount > 0) {
            //    if (Input.GetTouch(0).phase == TouchPhase.Began) {
            //        TakeScreenshot();
            //    }
            //}

        }
    }

    void TakeScreenshot() {

        System.DateTime dt = System.DateTime.Now;
        Application.CaptureScreenshot(string.Format("Screenshot_{0}{1}{3}{4}{5}.png", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Millisecond));
    }

}
