﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class FlyingSantaController : MonoBehaviour {

    [Header("出現間隔の最小値はアニメーションの時間と同じ値でおねがいします")]
    [SerializeField] float minSantaComingInterval;
    [SerializeField] float maxSantaComingInterval;

    const string animatorTriggerKey = "Come";

    Animator animator;

    void Start() {
        animator = GetComponent<Animator>();
        StartCoroutine(SantaControllCoroutine());
    }

    IEnumerator SantaControllCoroutine() {
        while (true) {

            animator.SetTrigger(animatorTriggerKey);

            yield return new WaitForSeconds(Random.Range(minSantaComingInterval, maxSantaComingInterval));
        }
    }

}
